import logging

from telegram import InlineKeyboardButton, bot
from telegram.ext import *
from exle_service import *

API_KEY = '5221239862:AAG9A6VwvfCAGYWi4tDioXvwlk7teYpkT-0'
updater = Updater(API_KEY, use_context=True)
dp = updater.dispatcher


print('Starting Bot...')

def city(update,context):
    list_of_cities = ['Erode', 'london']
    button_list = []
    for each in list_of_cities:
        button_list.append(InlineKeyboardButton(each, callback_data = each))
        reply_markup=InlineKeyboardButton(build_menu(button_list, n_cols=1))
        bot.send_message(chat_id=update.message.chat_id, text='choose from the list' , reply_markup=reply_markup)

def build_menu(buttons,n_cols,header_buttons=None,footer_buttons=None) :
    menu = [buttons[i:i + n_cols] for i in range(0,len(buttons),n_cols)]
    if header_buttons:
        menu.insert(0,header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
        return menu



def start_command(update, context):
    update.message.reply_text("""اهلا بكم في جمعية درة الشام السكنية:
    /begain - للبدأ
    """)


def begain_command(update, context):
    update.message.reply_text("""المشاريع المتاحة: 
    /Baitalsham - للدخول الى مشروع بيت الشام
    /Douretalfae7aa - للدخول الى مشروع درة الفيحاء
    /saharaalsham - للدخول الى مشروع صحارى الشام
    /koussoralsham - للدخول الى مشروع قصور الشام""")


def baitalsham_command(update, context):
    update.message.reply_text("""اهلا بك في مشروع بيت الشام السكني :
/information_baitalsham - للحصول على معلومات خاصة بالمكتتب
/payments_baitalsham - للاستعلام عن الدفعات
/apartment_baitalsham -  للاستعلام عن وضع الشقة الخاصة بالمكتتب
    """)

def informationBaitalsham_command(update, context):
    update.message.reply_text("""اهلا بك في جمعية درة الشام مشروع بيت الشام السكني """)
    update.message.reply_text(""" أدخل رقم العضوية رجاءً """)
    dp.add_handler(MessageHandler(Filters.text, user_info))
def paymentsBaitalsham_command(update, context):
    update.message.reply_text("""اهلا بك في جمعية درة الشام مشروع بيت الشام السكني """)
    update.message.reply_text(""" أدخل رقم العضوية رجاءً """)
    dp.add_handler(MessageHandler(Filters.text, payments_info))



def douretalfae7aa_command(update, context):
    update.message.reply_text("""اهلا بك في مشروع درة الفيحاء :
/information - للحصول على معلومات خاصة بالمكتتب
/payments - للاستعلام عن الدفعات
/apartment -  للاستعلام عن وضع الشقة الخاصة بالمكتتب
    """)


def saharaalsham_command(update, context):
    update.message.reply_text("""اهلا بك في مشروع صحارى الشام :
/information - للحصول على معلومات خاصة بالمكتتب
/payments - للاستعلام عن الدفعات
/apartment -  للاستعلام عن وضع الشقة الخاصة بالمكتتب
        """)


def koussoralsham_command(update, context):
    update.message.reply_text("""اهلا بك في مشروع قصور الشام :
/information - للحصول على معلومات خاصة بالمكتتب
/payments - للاستعلام عن الدفعات
/apartment -  للاستعلام عن وضع الشقة الخاصة بالمكتتب
            """)


# def custom_command(update, context):
#     update.message.reply_text('This is a custom command, you can add whatever text you want here.')

def user_info(update, context):
    text = str(update.message.text).lower()
    result = getInfo(text)
    update.message.reply_text(""" معلومات العضو """)
    update.message.reply_text(result)
    update.message.reply_text("""اهلا بكم في جمعية درة الشام السكنية:
        /begain - للبدأ
        """)
def payments_info(update, context):
    text = str(update.message.text).lower()
    result1 = getPayments(text)
    update.message.reply_text(""" معلومات حول الدفعات الخاصة بالمكتتب """)
    update.message.reply_text(result1)
    update.message.reply_text("""اهلا بكم في جمعية درة الشام السكنية:
        /begain - للبدأ
        """)

def error(update, context):
    # Logs errors
    print(f'Update {update} caused error {context.error}')


# Run the programme
if __name__ == '__main__':


    # Commands
    dp.add_handler(CommandHandler('start', start_command))
    dp.add_handler(CommandHandler('begain', begain_command))
    dp.add_handler(CommandHandler('payments_baitalsham',paymentsBaitalsham_command))
    dp.add_handler(CommandHandler('Baitalsham', baitalsham_command))
    dp.add_handler(CommandHandler('information_baitalsham', informationBaitalsham_command))
    dp.add_handler(CommandHandler('Douretalfae7aa', douretalfae7aa_command))
    dp.add_handler(CommandHandler('saharaalsham', saharaalsham_command))
    dp.add_handler(CommandHandler('koussoralsham', koussoralsham_command))

    # Messages

    # Log all errors
    dp.add_error_handler(error)

    # Run the bot
    updater.start_polling(0.5)
    updater.idle()